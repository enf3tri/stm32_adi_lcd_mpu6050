# STM32F429ZI Attitude Indicator (ADI) with MPU6050 Gyro Sensor and Nokia 5110 LCD Screen

[![Video](https://img.youtube.com/vi/iSLv_3tGg7c/0.jpg)](https://www.youtube.com/watch?v=iSLv_3tGg7c)

This project is an Attitude Indicator (ADI) implemented on the STM32F429ZI microcontroller using an MPU6050 gyro sensor and a Nokia 5110 LCD screen. The ADI provides pitch information, displaying the aircraft's attitude. This can be useful for various applications, including remote-controlled aircraft, drones, or any other project where attitude information is essential.

## Features

- Real-time attitude indication in the pitch axis.
- Utilizes the MPU6050 gyro sensor for precise orientation data.
- Graphical representation on the Nokia 5110 LCD screen.
- STM32F429ZI microcontroller for processing and display control.
- Minimal external components required for the setup.

## Inspiration from Aviation ADI Systems

The Attitude Indicator (ADI) is a critical instrument in aviation used to display an aircraft's attitude relative to the horizon. While this project is designed for hobbyist and educational purposes, it draws inspiration from advanced ADI systems used in commercial and military aircraft like the Boeing 787 and F-16. Here are some features that real aviation ADIs incorporate:

### Boeing 787 Dreamliner

![Boeing 787 Dreamliner ADI](https://gitlab.com/enf3tri/stm32_adi_lcd_mpu6050/-/raw/main/Images/adi_1.jpg?ref_type=heads)

### F-16 Fighting Falcon

![F-16 ADI](https://gitlab.com/enf3tri/stm32_adi_lcd_mpu6050/-/raw/main/Images/adi_2.jpg?ref_type=heads)


## Hardware and Sensors

- STM32F429ZI microcontroller board.
- MPU6050 gyro sensor module.
- Nokia 5110 LCD screen.
- Breadboard and jumper wires for connecting the components.
- Power supply for the STM32F429ZI.

## Setup

![Wiring](https://gitlab.com/enf3tri/stm32_adi_lcd_mpu6050/-/raw/main/Images/5110ADI.png?ref_type=heads)

1. **Wiring:**

   - Connect the MPU6050 to the STM32F429ZI using I2C communication.
   - Wire the Nokia 5110 LCD screen to the STM32F429ZI as per the datasheet or provided pinout.

2. **Software Configuration:**

   - Set up the STM32F429ZI development environment using your preferred toolchain 
   - Import the provided code repository and configure your project to include the necessary libraries and drivers.

3. **Code Configuration:**

   - Modify the code to customize settings and calibration values as per your requirements.
   - Ensure that the MPU6050 is properly calibrated for accurate attitude data.

4. **Compile and Flash:**

   - Compile your project and flash the STM32F429ZI microcontroller with the firmware.

5. **Testing:**

   - Power up the system and observe the attitude indicator on the Nokia 5110 LCD screen.
   - Verify that the roll indication responds accurately to changes in orientation along the Y-axis.

## Usage

This ADI is designed to provide real-time attitude information primarily along the Y-axis (roll). You can integrate it into your project to monitor and display the orientation of your device or vehicle. Ensure that you have a stable power supply and that the MPU6050 sensor is correctly calibrated for optimal performance.


